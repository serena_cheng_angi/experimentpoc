require 'ldclient-rb'

class Articles2Controller < ApplicationController
  CTRL_RESULT = "ctrl"
  TEST_RESULT = "test"
  BETTER_TEST_RESULT = "better_test"
  METRIC_KEY = "ruby_sdk_metric"

  def index
    user_key = "user" + rand(1000).to_s + "@angi.com"
    user = {
      key: "user-key-123abc",
      region: "region-nyc"
    }
    data = {
      user_decision: rand(0..1)
    }
    result = client.variation("ruby_sdk_ctrl", user, false)

    @greetings = greetings(result)
    client.track(METRIC_KEY, user, data, rand(1000...10000))
    display_ff
  end

  def display_ff

    user_key = "user_ff_test@angi.com"

    user = {
      key: "user-key-123abc",
      region: "region-nyc"
    }
    result = client.variation("region-specific-ff", user, false)

    @ff_region_enabled_greetings = if result == true then "ON" else "OFF" end
    puts result
  end

  def greetings(result)
    if result == TEST_RESULT
      "Hello!"
    elsif result == CTRL_RESULT
      "Goodbye!"
    elsif result == BETTER_TEST_RESULT
      "Adio!"
    end
  end

  def client
    Rails.configuration.client
  end
end
