require "splitclient-rb"

class ArticlesController < ApplicationController
  CUSTOMER_ID = "USER_ID"
  SPLIT_NAME = "ruby_sdk"
  CTRL_RESULT = "ctrl"
  TEST_RESULT = "test"


  def index
    customer_id = CUSTOMER_ID + rand(10000).to_s
    result = split_client.get_treatment(customer_id, SPLIT_NAME)
    @greetings = greetings(result)

    user_decision = rand(1000..10000)
    event_metadata = {
      region: 'region-la',
      sessionId: customer_id,
      tier: '2'
    }
    split_client.track(customer_id, "user", "user_decision", user_decision, event_metadata)
  end

  def greetings(result)
    if result == TEST_RESULT
      "Hello!"
    elsif result == CTRL_RESULT
      "Goodbye!"
    end
  end

  def split_client
    Rails.configuration.split_client
  end
end
